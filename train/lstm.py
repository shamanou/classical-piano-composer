""" This module prepares midi file data and feeds it to the neural
    network for training """

import os
import pickle

import numpy
from google.cloud import storage
from keras.callbacks import ModelCheckpoint
from keras.layers import Activation, Reshape
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.models import Sequential
from keras.utils import np_utils
from music21 import converter, instrument, note, chord

credentials = {"key": "AIzaSyBRgU4qkesmZ8LucefMFuO_aZs5sg2b5kI"}
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "../credentials.json"


def list_blobs_with_prefix(bucket_name, prefix):
    """Lists all the blobs in the bucket that begin with the prefix.
    This can be used to list all blobs in a "folder", e.g. "public/".
    The delimiter argument can be used to restrict the results to only the
    "files" in the given "folder". Without the delimiter, the entire tree under
    the prefix is returned. For example, given these blobs:
        /a/1.txt
        /a/b/2.txt
    If you just specify prefix = '/a', you'll get back:
        /a/1.txt
        /a/b/2.txt
    However, if you specify prefix='/a' and delimiter='/', you'll get back:
        /a/1.txt
    """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blobs = bucket.list_blobs(prefix=prefix)

    print('Blobs:')
    out = []
    for blob in blobs:
        out.append(download_blob('midi_shamanou', blob.name))

    return out


def download_blob(bucket_name, source_blob_name):
    """Downloads a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    print('Blob {} downloaded.'.format(source_blob_name))

    return blob


# [START storage_upload_file]
def upload_blob(bucket_name, source_file, destination_blob_name):
    """Uploads a file to the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_string(source_file)

    print('File uploaded to {}.'.format(destination_blob_name))


def train_network(request):
    """ Train a Neural Network to generate music """
    get_instruments()
    notes = get_notes()

    # get amount of pitch names
    n_vocab = [len(set(instrument)) for instrument in notes]

    network_input, network_output = prepare_sequences(notes, n_vocab)
    model = create_network(network_input, n_vocab)
    train(model, network_input, network_output)


def get_instruments():
    instruments = []

    for file in list_blobs_with_prefix("midi_shamanou", "new_midi_songs"):
        midi = converter.parse(file)

        print("Parsing %s" % file)

        partitioned_by_instruments = instrument.partitionByInstrument(midi)
        instruments_in_mid = list(partitioned_by_instruments.parts)

        for instrument_in_mid in instruments_in_mid:
            if instrument_in_mid not in instruments:
                instruments.append(instrument_in_mid)

        pickled = pickle.dumps(instruments)
        upload_blob("midi_shamanou", pickled)

    return instruments


def get_notes():
    """ Get all the notes and chords from the midi files in the ./midi_songs directory """
    notes = []

    for file in list_blobs_with_prefix("midi_shamanou", "new_midi_songs"):
        midi = converter.parse(file)

        print("Parsing %s" % file)

        notes_to_parse = []

        partitioned_by_instruments = instrument.partitionByInstrument(midi)
        for instrument_element_in_mid in partitioned_by_instruments.parts:
            tmp = []
            for instrument_in_mid in instrument_element_in_mid:
                tmp.append(instrument_in_mid)
            notes_to_parse.append(tmp)

        for parsed_instrument in notes_to_parse:
            tmp = []
            for element in parsed_instrument:
                if isinstance(element, note.Note):
                    tmp.append(str(element.pitch))
                elif isinstance(element, chord.Chord):
                    tmp.append('.'.join(str(n) for n in element.normalOrder))
            notes.append(tmp)
    notes = pickle.dumps(notes)
    upload_blob("midi_shamanou", notes, 'notes')

    return notes


def prepare_sequences(notes, n_vocab):
    """ Prepare the sequences used by the Neural Network """
    sequence_length = 100

    # get all pitch names
    pitch_names = []
    for instrument_in_mid in notes:
        pitch_names.append(sorted(set(item for item in instrument_in_mid)))

    # create a dictionary to map pitches to integers
    note_to_int = []
    for instrument_in_mid in pitch_names:
        note_to_int.append(dict((note, number) for number, note in enumerate(instrument_in_mid)))

    network_input = [[]] * len(notes)
    network_output = [[]] * len(notes)

    # create input sequences and the corresponding outputs
    for instrument_index, instrument_in_mid in enumerate(notes):
        for i in range(0, len(instrument_in_mid) - sequence_length, 1):
            sequence_in = instrument_in_mid[i:i + sequence_length]
            sequence_out = instrument_in_mid[i + sequence_length]
            network_input[instrument_index].append([note_to_int[instrument_index][char]
                                                    for char in sequence_in])
            network_output[instrument_index].append(note_to_int[instrument_index][sequence_out])

    network_input = numpy.array(network_input, dtype=numpy.float32)
    network_input = network_input.reshape((network_input.shape[1], network_input.shape[0], -1))
    network_output = numpy.array(network_output, dtype=numpy.float32)
    network_output = network_output.reshape((network_output.shape[1], network_output.shape[0], -1))

    n_patterns = len(network_input)

    # reshape the input into a format compatible with LSTM layers
    network_input = network_input.reshape(n_patterns, sequence_length, network_input.shape[1])
    # normalize input
    for i, vocab in enumerate(n_vocab):
        network_input[i] = network_input[i] / vocab

    network_output = np_utils.to_categorical(network_output)

    return network_input, network_output


def create_network(network_input, n_vocab):
    """ create the structure of the neural network """
    model = Sequential()
    model.add(LSTM(
        512,
        input_shape=(network_input.shape[1], network_input.shape[2]),
        return_sequences=True
    ))
    model.add(Dropout(0.3))
    model.add(LSTM(512, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(LSTM(512))
    model.add(Dense(256))
    model.add(Dropout(0.3))
    model.add(Dense(len(n_vocab) * max(n_vocab)))
    model.add(Reshape((len(n_vocab), max(n_vocab))))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

    return model


def store_result(model, weights_filename):
    model = model.to_json
    model_handle = open('model.json')
    model_handle.write(model)

    upload_blob("midi_shamanou", weights_filename, weights_filename)
    upload_blob("midi_shamanou", 'model.json', 'model.json')


def train(model, network_input, network_output):
    """ train the neural network """
    filepath = "weights-improvement-bigger.hdf5"
    checkpoint = ModelCheckpoint(
        filepath,
        monitor='loss',
        verbose=0,
        save_best_only=True,
        mode='min'
    )
    callbacks_list = [checkpoint]

    model.fit(network_input, network_output, epochs=2, batch_size=150, callbacks=callbacks_list)
    store_result(model, filepath)
