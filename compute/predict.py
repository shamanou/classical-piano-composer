""" This module generates notes for a midi file using the
    trained neural network """
import pickle

import numpy
from google.cloud import storage
from keras.models import model_from_json
from music21 import note, stream, chord


def download_blob(bucket_name, source_blob_name, destination_file_name):
    """Downloads a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    print('Blob {} downloaded to {}.'.format(
        source_blob_name,
        destination_file_name))


def generate():
    """ Generate a piano midi file """
    # load the notes used to train the model
    with open('notes', 'rb') as filepath:
        download_blob("midi_shamanou", filepath, filepath)
        notes = pickle.load(filepath)

    # Get all pitch names
    pitch_names = [sorted(set(item for item in instrument_in_mid)) for instrument_in_mid in notes]

    # Get all pitch names
    n_vocab = [len(set(instrument)) for instrument in notes]

    network_input, normalized_input = prepare_sequences(notes, pitch_names, n_vocab)
    model = create_network(normalized_input, n_vocab)
    prediction_output = generate_notes(model, network_input, pitch_names, n_vocab)
    create_midi(prediction_output)


def prepare_sequences(notes, pitchnames, n_vocab):
    """ Prepare the sequences used by the Neural Network """
    # map between notes and integers and back
    note_to_int = [dict((note, number)
                        for number, note in enumerate(instrument))
                   for instrument in pitchnames]

    sequence_length = 100
    network_input = [[]] * len(notes)
    output = [[]] * len(notes)
    for index, note_element in enumerate(notes):
        for i in range(0, len(note_element) - sequence_length, 1):
            sequence_in = note_element[i:i + sequence_length]
            sequence_out = note_element[i + sequence_length]
            network_input[index].append([note_to_int[index][char] for char in sequence_in])
            output[index].append(note_to_int[index][sequence_out])

    network_input = numpy.array(network_input, dtype=numpy.float32)
    network_input = network_input.reshape((network_input.shape[1], network_input.shape[0], -1))
    n_patterns = len(network_input)

    # reshape the input into a format compatible with LSTM layers
    normalized_input = numpy.reshape(network_input,
                                     (n_patterns, sequence_length, network_input.shape[1]))
    # normalize input
    for i, vocab in enumerate(n_vocab):
        normalized_input[i] = normalized_input[i] / vocab

    return network_input, normalized_input


def create_network(network_input, n_vocab):
    """ create the structure of the neural network """
    download_blob("shamanou_midi", "model.json", "model.json")
    download_blob("midi_shamanou", 'weights-improvement-bigger.hdf5', 'weights-improvement-bigger.hdf5')

    model = model_from_json(open('model.json').read())
    model.load_weights('weights-improvement-bigger.hdf5')

    return model


def generate_notes(model, network_input, pitch_names, n_vocab):
    """ Generate notes from the neural network based on a sequence of notes """
    # pick a random sequence from the input as a starting point for the prediction
    start = numpy.random.randint(0, network_input.shape[1] - 1)

    int_to_note = [dict((number, note)
                        for number, note in enumerate(instrument))
                   for instrument in pitch_names]
    pattern = []
    for instrument in range(network_input.shape[1]):
        pattern.append(network_input[instrument][start])
    prediction_output = [[]] * len(pitch_names)

    for _ in range(50):
        pattern = numpy.array(pattern, dtype=numpy.float32)
        prediction_input = numpy.reshape(pattern, (1, pattern.shape[1], len(pitch_names)))
        pattern = pattern.tolist()
        for index, prediction_input_element in enumerate(prediction_input):
            prediction_input[index] = prediction_input_element / float(n_vocab[index])

        prediction = model.predict(prediction_input, verbose=0)[0]
        for i, instrument in enumerate(int_to_note):
            index = numpy.argmax(prediction[i])
            if index in int_to_note[i]:
                prediction_output[i].append(instrument[index])
                pattern[index].append(index)
                pattern[index] = pattern[index][1:len(pattern[index])]

    return prediction_output


def create_midi(prediction_output):
    """ convert the output from the prediction to notes and create a midi file
        from the notes """
    offset = 0
    output_notes = []

    with open('instruments', 'rb') as filepath:
        download_blob("midi_shamanou", filepath, filepath)
        instruments = pickle.load(filepath)

    prediction_output = numpy.array(prediction_output, dtype=numpy.str)
    prediction_output = prediction_output.reshape((prediction_output.shape[1], -1))

    # create note and chord objects based on the values generated by the model
    for prediction in prediction_output:
        for index, pattern in enumerate(prediction):
            # pattern is a chord
            if ('.' in pattern) or pattern.isdigit():
                notes_in_chord = pattern.split('.')
                notes = []
                for current_note in notes_in_chord:
                    new_note = note.Note(int(current_note))
                    new_note.storedInstrument = instruments[index]
                    notes.append(new_note)
                new_chord = chord.Chord(notes)
                new_chord.offset = offset
                output_notes.append(new_chord)
            # pattern is a note
            else:
                new_note = note.Note(pattern)
                new_note.offset = offset
                new_note.storedInstrument = instruments[index]
                output_notes.append(new_note)

        # increase offset each iteration so that notes do not stack
        offset += 0.5

    midi_stream = stream.Stream(output_notes).chordify()

    midi_stream.write('midi', fp='test_output.mid')
